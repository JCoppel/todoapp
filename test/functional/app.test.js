const puppeteer = require('puppeteer');

(async () => {
    const browser = await puppeteer.launch({
        headless: false,
        slowMo: 25,
    });
    console.log('browser ok');
    const page = await browser.newPage();
    console.log('page ok');

    const url = 'file:///C:/Users/julie/OneDrive/Bureau/Julien/Cours%20ORT/4Olen/IndustrialisationDuDeveloppementEtTests/Exercices/todoapp/ToDoApp2/index.html';

    await page.goto(url);
    console.log('site web ok');

    await page.waitForSelector('#taskInput');
    console.log('input ok');

    for(let i = 0; i < 5; i++) {
        await page.type('#taskInput', 'Concombre');
        console.log('task ok');

        await page.click('#addTask');
        console.log('add ok');
    }

    await page.click('.removeTask');
    console.log('remove ok');

    await new Promise(r => setTimeout(r, 3000));

    await browser.close();
})();
