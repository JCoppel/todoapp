const jsdom = require('jsdom');
const { JSDOM } = jsdom;
const assert = require('assert');
const { TodoApp, Store } = require('../../src/app');

const dom = new JSDOM('<!doctype html><html><body></body></html>', {
    url: 'http://localhost:63342/todoapp/index.html',
    storageQuota: 10000000,
});

global.localStorage = dom.window.localStorage;

describe('TodoApp', () => {
    let todoApp;
    let store;

    beforeEach(() => {
        store = new Store();
        todoApp = new TodoApp(store);
    });

    afterEach(() => {
        localStorage.clear();
    });

    describe('addTask', () => {
        it('devrait ajouter une tâche à la liste des tâches', () => {
            todoApp.addTask('Tâche 1');
            assert.deepEqual(todoApp.getTasks(), ['Tâche 1']);
        });

        it('devrait enregistrer la tâche dans le store', () => {
            todoApp.addTask('Tâche 1');
            assert.deepEqual(store.getData(), ['Tâche 1']);
        });
    });

    describe('removeTask', () => {
        beforeEach(() => {
            todoApp.addTask('Tâche 1');
            todoApp.addTask('Tâche 2');
            todoApp.addTask('Tâche 3');
        });

        it('devrait supprimer une tâche de la liste des tâches', () => {
            todoApp.removeTask(1);
            assert.deepEqual(todoApp.getTasks(), ['Tâche 1', 'Tâche 3']);
        });

        it('devrait enregistrer la liste de tâches mise à jour dans le store', () => {
            todoApp.removeTask(1);
            assert.deepEqual(store.getData(), ['Tâche 1', 'Tâche 3']);
        });
    });

    describe('getTasks', () => {
        it('devrait renvoyer un tableau vide s\'il n\'y a pas de tâches', () => {
            assert.deepEqual(todoApp.getTasks(), []);
        });

        it('devrait renvoyer la liste des tâches', () => {
            todoApp.addTask('Tâche 1');
            todoApp.addTask('Tâche 2');
            assert.deepEqual(todoApp.getTasks(), ['Tâche 1', 'Tâche 2']);
        });
    });
});
